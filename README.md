# Python tutorial workspace

The projects in this group are sample codes from the tutorial in the official Python 3 documentation.

* [The Python Tutorial](https://docs.python.org/3/tutorial/index.html)


## Notes

You can use vaiours text editors or IDEs to edit Python code.
I personally use Visual Studio Code with Python extension.

Here's how to do it:

* [Python on Visual Studio Code](https://code.visualstudio.com/docs/languages/python)


